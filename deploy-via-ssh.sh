#!/usr/bin/env sh

# Deploy the app on a distant machine
# Requires arguments: HOST SSH_USER SSH_KEY
# Example: ./deploy-via-ssh.sh 1.2.3.4 ubuntu /path/to/ssh.key

DEPLOY_HOST=$1
DEPLOY_SSH_USER=$2
DEPLOY_SSH_KEY=$3

# Create directory for docker-compose
# Workaround for https://github.com/docker/compose/issues/1973
# docker-compose up --no-build will complain because build directories does not exists otherwise
ssh -i ${DEPLOY_SSH_KEY} ${DEPLOY_SSH_USER}@${DEPLOY_HOST} -- mkdir -p /home/${DEPLOY_SSH_USER}/worker
ssh -i ${DEPLOY_SSH_KEY} ${DEPLOY_SSH_USER}@${DEPLOY_HOST} -- mkdir -p /home/${DEPLOY_SSH_USER}/vote
ssh -i ${DEPLOY_SSH_KEY} ${DEPLOY_SSH_USER}@${DEPLOY_HOST} -- mkdir -p /home/${DEPLOY_SSH_USER}/result

# Copy deployment files
scp -i ${DEPLOY_SSH_KEY} *.sh *.yml .env ${DEPLOY_SSH_USER}@${DEPLOY_HOST}:/home/${DEPLOY_SSH_USER}

# Run Docker Compose
ssh -i ${DEPLOY_SSH_KEY} ${DEPLOY_SSH_USER}@${DEPLOY_HOST} -- docker-compose pull
ssh -i ${DEPLOY_SSH_KEY} ${DEPLOY_SSH_USER}@${DEPLOY_HOST} -- docker-compose up --no-build -d